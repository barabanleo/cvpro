import React from 'react';


import styled from 'styled-components'
import PropTypes from 'prop-types';


import Droppable from "../Droppable";
import Draggable from "../Draggable"



const Wrapper = styled.div`
  width : 100%,
  padding : 32 px;
  display : flex ;
  justify-content : center;
`;



const Item = styled.button`
  padding : 8 px;
  color : #555;
  background-color : white; 
  border-radius : 3px;
`;


const droppableStyles={

  backgroundColor :'#555',
  width : '250px' ,
  heigth :'400px',
  margin : '32px'

};


export default class DndTest extends React.Component {


  render(){
    return(

        <Wrapper>
          <Droppable id="dr1" style={droppableStyles}>
            <Draggable id="item1" style={{margin : '8px'}}><Item>My first Item</Item></Draggable>
            <Draggable id="item2" style={{margin : '8px'}}><Item>My second Item</Item></Draggable>
          </Droppable>


          <Droppable id="dr2" style={droppableStyles}>
          </Droppable>
        </Wrapper>

    )

  }

}
