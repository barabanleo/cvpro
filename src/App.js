import React from 'react';
import './App.css';

import About from './About.js';
import Nav from './Nav.js';
import Shop from './Shop.js';
import createCv from './createCv.js';



import styled from 'styled-components';
import DndTest from './Dnd/DndTest';



import {BrowserRouter as Router , Switch , Route} from 'react-router-dom';


const AppWrapper = styled.div`
  display : flex ;
  justify-content : center;
  margin-top : 100px;
`;

const Container = styled.div`
`;




function App() {
  return (

        <div>
        <Router>
          <div className="App">
            <Nav />
            <Route path="/about" component={About}/>
            <Route path="/shop" component={Shop}/>
            <Route path="/cv" component={createCv}/>
          </div>
        </Router>

            <AppWrapper>
                <Container>
                    <DndTest />
                </Container>
            </AppWrapper>
        </div>
  );
}

export default App;
